# WORCK DKAN publication workflow

This document describes the intended DKAN publication process for the developed publication formats (dataset, data story, ...).

## Submission and publication dates

The submission of the data to be published should be made **at least six working days before the targeted publication date**. In case of an extraordinarily high number of data, unusually complex implementations or unclear formulations, a delay in publication will be announced as soon as possible. Any ambiguities will always be discussed with the editorial team.

## Data transfer modalities

To maintain clarity, the submission should be sent in the following format in **final version only** (email attachment or cloud storage link).

- 1x Markdown/PDF/Word file that meets the following requirements:
- contains text formatting
    - uses the correct markup for headings
    - final positioning of illustrations
    - Use hyperlinks instead of notes like "insert link to ..."
    - Add iframes with the following code: `%iframe=https://www.example.com%`
- 1x folder “images” with images (max. 2MB per file, only .png and .jpg)
- 1x folder „assets“ for other files like HTML-Maps (max. 2MB per file)
- 1x metadata file "metadata.txt" with a listing of the desired tags, topics and a summary (see texts under Data Story Overview)
- 1x headerimage „header.jpg/.png“ (optional)

## Publication

After the data has been entered into the system, a review loop takes place. A **pre-release version**, which cannot be viewed publicly, **is made available to the editorial team**, which communicates any corrections back or publishes the final data.

## Review process

1. Log in with the reviewer account
2. Open the resource (Data Story, Bibliography, ...) link provided by Team Bielefeld
3. Check the preview version for errors
4. If errors are found, try to fix them or write back an email with all problems
  1. **Error in Title**: Edit -> Title field -> make changes -> **save**
  2. **Error in text block**: Customize Display -> Operations content -> select the settings wheel of the text block -> edit -> make changes -> Finish -> **save**
    1. Fix the errors with either Markdown HTML or Full HTML (based on the Text format indicator underneath the Text input areas)
5. If the preview version contains no more bugs, publish it: Edit -> Scroll Down to "Publishing options" -> Check "Published" -> **save**
6. Check if the published version corresponds to the preview
7. If there are differences, then unpublish the resource and write back an email

### Markdown HTML Basics

- _Italic text_ with `*asterisks*`, `_underscores_` or `<i>HTML tags</i>`.
- **Strong emphasis** with `**asterisks**`, `__underscores__` or `<b>HTML tags</b>`.
- [Links](https://www.example.com) with `[link text](https://www.example.com)` or `<a href="https://www.example.com">link text</a>`

### Full HTML basics

```
<p>This is a paragraph</p>
<p>This is a paragrap<br>with a forced new line</p>
<p style="color: green;">This is a paragraph containing green text</p>
<p style="margin-left: 10%; width: 80%;">This is a centered paragraph with 80% of the available width</p>
This is a <span style="color: green;">green</span> word
This is an <i>italic</i> word
This is a <b>bold</b> word
This is a <a href="https://www.example.com">link</a>
This is a <a title="This text is shown on mouseover">tooltip</a>

```
